import {Component, OnInit} from '@angular/core';
import {ProductModel} from "../../shared/model/product.model";
import {WarehouseModel} from "../../shared/model/warehouse.model";
import {WarehouseService} from "../../shared/service/warehouse.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-warehouse',
  templateUrl: './warehouse.component.html',
  styleUrls: ['./warehouse.component.css']
})
export class WarehouseComponent implements OnInit {
  warehouse: Array<WarehouseModel> = [];

  constructor(private warehouseService: WarehouseService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.warehouse = this.route.snapshot.data['warehouse'];
    console.log(this.warehouse)
  }


}
